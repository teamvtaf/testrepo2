package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class HomePage implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum homePage {

        ele_ddAllCategories("//select[@id='gh-cat']"), btn_categorySearch("//input[@id='gh-btn']"), ele_lblWelcomeTextHi("//a[contains(text(),'Hi')]"), ele_lblSelectedCategory("//h1[contains(text(),'<idtf_selectedCategory>')]"), ele_lblFirstItemInTheResult("//h3/a[contains(text(),'<idtf_productName>')]"), ele_lblFirstSubCategory("//a[@class='title extended-links']"), ele_lblFirstResultOfSubCategory("xpath=(//ul[contains(@class,'navigation-list nested add-it')])[1]"), lnk_firstItemInTheResult("//li[contains(@id,'item')]//h3/a"), ele_productHeadingToVerifyWithText("//h1[contains(text(),'<idtf_productName>')]"), lnk_SighnOut("//a[contains(text(),'Sign out')]");

    private String searchPath;
  
    /**
    *  Page homePage.
    */
    private homePage(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}