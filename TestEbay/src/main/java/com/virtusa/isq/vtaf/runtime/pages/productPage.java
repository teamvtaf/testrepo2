package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class ProductPage implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum productPage {

        btn_addToCart("//a[@id='isCartBtn_btn']"), ele_lblproductNameInVerificationMessage("//a[contains(text(),'<idtf_productName>')]"), lnk_removeFromCart("//a[contains(text(),'Remove')]"), ele_lblAddedToCartMessage("//span[contains(text(),'was just added to your cart')]"), ele_lblRemovedFromCartMessage("//span[contains(text(),'was removed from your cart')]");

    private String searchPath;
  
    /**
    *  Page productPage.
    */
    private productPage(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}