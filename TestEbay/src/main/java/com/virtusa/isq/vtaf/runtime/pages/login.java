package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class Login implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum login {

        img_ebayLogo("//a[@href='http://www.ebay.com/']"), tf_userName("xpath=(//input[@placeholder='Email or username'])[2]"), tf_password("//input[@placeholder='Password']"), btn_sighnIn("//input[@value='Sign in']");

    private String searchPath;
  
    /**
    *  Page login.
    */
    private login(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}