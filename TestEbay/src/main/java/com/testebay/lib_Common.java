package com.testebay;

import com.virtusa.isq.vtaf.runtime.SeleniumTestBase;
import org.openqa.selenium.By;
import com.virtusa.isq.vtaf.utils.PropertyHandler;

/**
 *  Class lib_Common contains reusable business components 
 *  Each method in this class correspond to a reusable business component.
 */
public class lib_Common {

    /**
     *  Business component bc_Login.
     * 
     */
    public final static void bc_Login(final SeleniumTestBase caller, final String prm_UserName, final String prm_Password) throws Exception {
        //Login into the application
        caller.open("https://signin.ebay.com/ws/eBayISAPI.dll?SignIn&ru=http%3A%2F%2Fwww.ebay.com%2F","3000");
        caller.checkElementPresent("login.img_ebayLogo",false,"");
        caller.type("login.tf_userName",prm_UserName);
        caller.type("login.tf_password",prm_Password);
        caller.click("login.btn_sighnIn");	
    }
    /**
     *  Business component bc_SelectCategoryHomePageTop.
     * 
     */
    public final static void bc_SelectCategoryHomePageTop(final SeleniumTestBase caller, final String prm_Category) throws Exception {
        //select main category
        caller.checkElementPresent("homePage.ele_lblWelcomeTextHi",false,"");
        caller.select("homePage.ele_ddAllCategories",prm_Category);
        caller.click("homePage.btn_categorySearch");	
    }
    /**
     *  Business component bc_StoreProductHeaderName.
     * 
     */
    public final static void bc_StoreProductHeaderName(final SeleniumTestBase caller, final String prm_TCName) throws Exception {
        //Store the product name
        String var_productName = caller.getStringProperty("homePage.lnk_firstItemInTheResult","TEXT:");
        caller.store(prm_TCName+"key_productName","String",var_productName);	
    }
    /**
     *  Business component bc_clickProduct.
     * 
     */
    public final static void bc_clickProduct(final SeleniumTestBase caller, final String prm_ProductName) throws Exception {
        //Click the selected product
        caller.click("homePage.ele_lblFirstItemInTheResult","idtf_productName_PARAM:" + prm_ProductName);	
    }
    /**
     *  Business component bc_VerifyProductName.
     * 
     */
    public final static void bc_VerifyProductName(final SeleniumTestBase caller, final String prm_productName) throws Exception {
        //Veridy product Name
        caller.checkElementPresent("homePage.ele_lblSelectedCategory","idtf_selectedCategory_PARAM:" + prm_productName,false,"");	
    }
    /**
     *  Business component bc_AddToCart.
     * 
     */
    public final static void bc_AddToCart(final SeleniumTestBase caller, final String prm_ProductName) throws Exception {
        //Add product to cart
        caller.click("productPage.btn_addToCart");
        caller.checkElementPresent("productPage.ele_lblproductNameInVerificationMessage","idtf_productName_PARAM:" + prm_ProductName,false,"");
        caller.checkElementPresent("productPage.ele_lblAddedToCartMessage",false,"");	
    }
    /**
     *  Business component bc_removeFromCart.
     * 
     */
    public final static void bc_removeFromCart(final SeleniumTestBase caller, final String prm_ProductName) throws Exception {
        //Remove product from cart
        caller.pause("3000","wait till page load");
        caller.click("productPage.lnk_removeFromCart");
        caller.checkElementPresent("productPage.ele_lblproductNameInVerificationMessage","idtf_productName_PARAM:" + prm_ProductName,false,"");
        caller.checkElementPresent("productPage.ele_lblRemovedFromCartMessage",false,"");	
    }
    /**
     *  Business component bc_LogOut.
     * 
     */
    /**
     *  Business component bc_LogOut.
     * 
     */
    public final static void bc_LogOut(final SeleniumTestBase caller) throws Exception {
        //Log out from the application
        caller.click("homePage.ele_lblWelcomeTextHi");
        caller.click("homePage.lnk_SighnOut");	
    }
}
