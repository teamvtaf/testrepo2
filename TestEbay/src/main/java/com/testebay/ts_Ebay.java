package com.testebay;

import java.util.HashMap;
import java.util.List;

import com.virtusa.isq.vtaf.aspects.VTAFRecoveryMethods;
import com.virtusa.isq.vtaf.runtime.SeleniumTestBase;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import com.virtusa.isq.vtaf.runtime.VTAFTestListener;
import com.virtusa.isq.vtaf.utils.PropertyHandler;


/**
 *  Class ts_Ebay implements corresponding test suite
 *  Each test case is a test method in this class.
 */

@Listeners (VTAFTestListener.class)
public class ts_Ebay extends SeleniumTestBase {



    /**
     * Data provider for Test case tc_cart_001.
     * @return data table
     */
    @DataProvider(name = "tc_cart_001")
    public Object[][] dataTable_tc_cart_001() {     	
    	return this.getDataTable("dt_tc_cart_001");
    }

    /**
     * Data driven test case tc_cart_001.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {}) 
    @Test (dataProvider = "tc_cart_001")
    public final void tc_cart_001(final String dt_tc_cart_001_TCName, final String dt_tc_cart_001_UserName, final String dt_tc_cart_001_Password, final String dt_tc_cart_001_MainCategory) throws Exception {
    	
    	//--Start of TestCase
    	lib_Common.bc_Login(this, dt_tc_cart_001_UserName,dt_tc_cart_001_Password);
    	//--Select main category and search
    	lib_Common.bc_SelectCategoryHomePageTop(this, dt_tc_cart_001_MainCategory);
    	//Navigate to product result page
    	lib_Navigation.bc_NavigatePainLeftSide(this);
    	//Store the product Name
    	lib_Common.bc_StoreProductHeaderName(this, dt_tc_cart_001_TCName);
    	//Retrieve product Name
    	String var_productName = retrieveString(dt_tc_cart_001_TCName+"key_productName");
    	//Click the first product in the result
    	lib_Common.bc_clickProduct(this, var_productName);
    	//Verify product name
    	lib_Common.bc_VerifyProductName(this, var_productName);
    	//Add product to cart
    	lib_Common.bc_AddToCart(this, var_productName);
    	//Remove product from cart
    	lib_Common.bc_removeFromCart(this, var_productName);
    	//Log out from the application
    	lib_Common.bc_LogOut(this);
    	
    } 
    	

    public final Object[][] getDataTable(final String... tableNames) {
        String[] tables = tableNames;
        return this.getTableArray(getVirtualDataTable(tables));
    }

}