package com.testebay;

import com.virtusa.isq.vtaf.runtime.SeleniumTestBase;
import org.openqa.selenium.By;
import com.virtusa.isq.vtaf.utils.PropertyHandler;

/**
 *  Class lib_Navigation contains reusable business components 
 *  Each method in this class correspond to a reusable business component.
 */
public class lib_Navigation {

    /**
     *  Business component bc_NavigatePainLeftSide.
     * 
     */
    /**
     *  Business component bc_NavigatePainLeftSide.
     * 
     */
    public final static void bc_NavigatePainLeftSide(final SeleniumTestBase caller) throws Exception {
        //Select category from the left navigation pane
        caller.click("homePage.ele_lblFirstSubCategory");
        caller.click("homePage.ele_lblFirstResultOfSubCategory");	
    }
}
